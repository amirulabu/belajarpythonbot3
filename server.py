import json
import os
from flask import Flask, request, Response, jsonify
import db_operation as db
from msg import Message
from callback_query import CallbackQuery
import config
from helper import ping_admin, logging
from quiz_answer import quiz_answer

app = Flask(__name__)

TOKEN = config.TELEGRAM_TOKEN
GROUP_ID = config.TELEGRAM_GROUP_ID


@app.route("/")
def index():
    return jsonify(statusCode=200, body="ok")


@app.route("/" + TOKEN, methods=["GET", "POST"])
def bot():
    if request.data:
        req = json.loads(request.data.decode("utf-8"))
        logging(req=req)
        if "callback_query" in req:
            msg = CallbackQuery(req)
        # if the chat is from the group
        elif req["message"]["chat"]["id"] < 1:
            logging(groupmessage=req)
            return jsonify(statusCode=200, body=json.dumps(req))
        else:
            msg = Message(req)
        quiz(msg)

        return jsonify(statusCode=200, body=json.dumps(req))

    else:
        logging(req=None)
        return jsonify(statusCode=200, body="ok")


def quiz(msg):
    logging(user=db.add_or_identify_user(msg.user))
    if "/start" in msg.text:
        ping_admin(f"{msg.full_name()} just started the test")
        msg.reply_message(
            f"Hello {msg.full_name()}, please answer the following questions"
        )
        msg.reply_message(quiz_answer["q1"], inline_keyboard=quiz_answer["a1"])
    elif "q1-w" in msg.text:
        answer = msg.text.split("-")
        logging(result=db.add_result(msg.user, answer[0], answer[1]))
        msg.edit_last_message()
        msg.reply_message(quiz_answer["q2"], inline_keyboard=quiz_answer["a2"])
    elif "q1-c" in msg.text:
        answer = msg.text.split("-")
        logging(result=db.add_result(msg.user, answer[0], answer[1]))
        msg.edit_last_message()
        msg.reply_message(quiz_answer["q2"], inline_keyboard=quiz_answer["a2"])
    elif "q2-w" in msg.text:
        answer = msg.text.split("-")
        logging(result=db.add_result(msg.user, answer[0], answer[1]))
        msg.edit_last_message()
        msg.reply_message(quiz_answer["q3"], inline_keyboard=quiz_answer["a3"])
    elif "q2-c" in msg.text:
        answer = msg.text.split("-")
        logging(result=db.add_result(msg.user, answer[0], answer[1]))
        msg.edit_last_message()
        msg.reply_message(quiz_answer["q3"], inline_keyboard=quiz_answer["a3"])
    elif "q3-w" in msg.text:
        answer = msg.text.split("-")
        logging(result=db.add_result(msg.user, answer[0], answer[1]))
        msg.edit_last_message()
        msg.reply_message(quiz_answer["q4"], inline_keyboard=quiz_answer["a4"])
    elif "q3-c" in msg.text:
        answer = msg.text.split("-")
        logging(result=db.add_result(msg.user, answer[0], answer[1]))
        msg.edit_last_message()
        msg.reply_message(quiz_answer["q4"], inline_keyboard=quiz_answer["a4"])
    elif "q4-w" in msg.text:
        answer = msg.text.split("-")
        logging(result=db.add_result(msg.user, answer[0], answer[1]))
        msg.edit_last_message()

        correct_ans = db.check_user_questions(msg.user)
        text = f"This is your result \nYou have {correct_ans} correct answers"
        if correct_ans == 4:
            link = "http://google.com"
            text = text + str(f"\nYou may enter the group with this link: {link}")
        else:
            text = text + str("\nSorry, please try again.")

        ping_admin(
            f"{msg.full_name()} have completed the test. Result: {correct_ans}/4"
        )
        msg.reply_message(text)
    elif "q4-c" in msg.text:
        answer = msg.text.split("-")
        logging(result=db.add_result(msg.user, answer[0], answer[1]))
        msg.edit_last_message()

        correct_ans = db.check_user_questions(msg.user)
        text = f"This is your result \nYou have {correct_ans} correct answers"
        if correct_ans == 4:
            link = db.save_or_update_group_link()
            text = text + str(f"\nYou may enter the group with this link: {link}")
        else:
            text = text + str("\nSorry, please try again. Click here /start")

        ping_admin(
            f"{msg.full_name()} have completed the test. Result: {correct_ans}/4"
        )
        msg.reply_message(text)
    else:
        logging(notmatch=msg.text)
        # this will repeat anything user text that does not match above
        # msg.reply_message(msg.text)
