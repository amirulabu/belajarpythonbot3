import requests
from helper import get_url


class CallbackQuery:
    """ Manage callback_query from the telegram user, when user clicks an inline button """

    def __init__(self, update):
        self.update = update
        self.user = update["callback_query"]["from"]
        self.callback_query = update["callback_query"]
        self.text = update["callback_query"]["data"]
        self.chat_id = update["callback_query"]["from"]["id"]
        self.first_name = update["callback_query"]["from"]["first_name"]
        # Telegram allow accounts without last name
        self.last_name = (
            update["callback_query"]["from"]["last_name"]
            if update["callback_query"]["from"].get("last_name")
            else None
        )
        # Telegram allow accounts without username
        self.username = (
            update["callback_query"]["from"]["username"]
            if update["callback_query"]["from"].get("username")
            else None
        )
        self.is_bot = update["callback_query"]["from"]["is_bot"]
        self.chat_instance = update["callback_query"]["chat_instance"]
        self.message = update["callback_query"]["message"]

    def full_name(self):
        return (
            "{} {}".format(self.first_name, self.last_name)
            if self.last_name
            else "{}".format(self.first_name)
        )

    def name_or_username(self):
        return self.username if self.username else self.first_name

    def reply_message(
        self, reply_text="I can hear you!", keyboard=None, inline_keyboard=None
    ):
        """
            Reply to the sender

                :param reply_text: text to reply
                :param keyboard: refer ReplyKeyboardMarkup type 
            https://core.telegram.org/bots/api/#replykeyboardmarkup
                :param inline_keyboard: refer InlineKeyboardMarkup type
            https://core.telegram.org/bots/api#inlinekeyboardmarkup
                :return: Response object from requests module
            http://docs.python-requests.org/en/master/api/#requests.Response
        """
        data = {}
        data["chat_id"] = self.chat_id
        data["text"] = reply_text
        if keyboard:
            data["reply_markup"] = {"keyboard": keyboard, "one_time_keyboard": True}
        elif inline_keyboard:
            data["reply_markup"] = {"inline_keyboard": inline_keyboard}
        r = requests.post(get_url("sendMessage"), json=data)
        return r

    def edit_last_message(self):
        requests.post(
            get_url("editMessageText"),
            json={
                "chat_id": self.message["chat"]["id"],
                "message_id": self.message["message_id"],
                "text": self.message["text"],
            },
        )

    def __repr__(self):
        return "CallbackQuery: {} - {}".format(self.text, self.name_or_username())
