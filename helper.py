import requests
from config import TELEGRAM_TOKEN, TELEGRAM_ADMIN


def send_message(response_dict):
    # response_dict = {"chat_id": chat_id, "text": text}
    # or
    # response_dict = {"chat_id": chat_id, "text": text, "reply_markup": reply_markup}
    req = requests.post(get_url("sendMessage"), json=response_dict)
    return req


def get_url(method):
    return "https://api.telegram.org/bot{}/{}".format(TELEGRAM_TOKEN, method)


def logging(**kwargs):
    for k, v in kwargs.items():
        print(k, "==>", v)


def ping_admin(text):
    for t in TELEGRAM_ADMIN:
        send_message({"chat_id": t, "text": text})
